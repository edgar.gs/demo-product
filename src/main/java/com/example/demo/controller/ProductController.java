package com.example.demo.controller;

import com.example.demo.entity.Product;
import com.example.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController()
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public List<Product> list() {
        return productService.list();
    }

    @GetMapping("/{productId}")
    public Product get(@PathVariable Long productId) {
        return productService.get(productId);
    }

    @PostMapping
    public Product create(@Valid @RequestBody Product product) {
        return productService.create(product);
    }

    @PutMapping("/{productId}")
    public Product update(@PathVariable Long productId, @Valid @RequestBody Product product) {
        return productService.update(product);
    }

    @DeleteMapping("/{productId}")
    public void delete(@PathVariable Long productId, @Valid @RequestBody Product product) {
        productService.delete(product);
    }
}
