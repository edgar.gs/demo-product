package com.example.demo.entity;

import org.hibernate.validator.constraints.URL;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Entity
public class Product {

    @Id
    @GeneratedValue()
    private Long id;

    @NotNull
    @Min(value = 1000000,message = "SKU should not be less than 1000000")
    @Max(value = 99999999,message = "SKU should not be greater than 99999999")
    private Long sku;

    @NotNull
    @NotBlank(message = "Name is mandatory")
    @Size(min = 3, max = 50, message
            = "Name must be between 3 and 50 characters")
    private String name;

    @NotNull
    @NotBlank(message = "Brand is mandatory")
    @Size(min = 3, max = 50, message
            = "Brand must be between 3 and 50 characters")
    private String brand;

    @NotBlank(message = "Size is mandatory")
    private String size;

    @NotNull
    @Min(value = 1,message = "Price should not be less than 1")
    @Max(value = 99999999,message = "Price should not be greater than 99999999")
    private Double price;

    @NotNull
    @URL()
    private String principalImage;

    @URL()
    private String otherImages;

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSku() {
        return sku;
    }

    public void setSku(Long sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPrincipalImage() {
        return principalImage;
    }

    public void setPrincipalImage(String principalImage) {
        this.principalImage = principalImage;
    }

    public String getOtherImages() {
        return otherImages;
    }

    public void setOtherImages(String otherImages) {
        this.otherImages = otherImages;
    }
}
