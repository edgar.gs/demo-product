package com.example.demo.services

import com.example.demo.entity.Product
import com.example.demo.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

class ProductServiceSpec extends Specification{

    @Shared
    ProductService productService
    @Shared
    ProductRepository productRepository = Mock()

    def setupSpec() {

        productRepository.findAll() >> []
        productRepository.findById(1) >> Optional.of(new Product())
        productRepository.save(_) >> new Product()

        productService = new ProductService(productRepository)
    }

    def "product list"() {
        when:
        def listProducts = productService.list()
        then:
        listProducts.size() == 0
    }

    def "product get"() {
        when:
        def product = productService.get(1)
        then:
        product
    }

    def "product create"() {
        when:
        def newProduct = productService.create(new Product())
        then:
        newProduct
    }

    def "product update"() {
        when:
        def newProduct = productService.update(new Product())
        then:
        newProduct
    }
}
